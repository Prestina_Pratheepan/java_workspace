package bcas.pro.str;

public class StringHomeWork {
	
		public static void main(String[] args){
			
			String fruit = "Apple";
			String color = "Red";
			String campus = "BCAS Campus ";
			String area = "Jaffna";
			String school = "   Royal Institute ";
			String hotel = "Valampuri";


			System.out.println(" QUESTION 1 ");
			System.out.println("output \t: " + fruit.charAt(2));
			System.out.println("*******************************");


			System.out.println(" QUESTION 5 ");
			System.out.println("output \t: " + campus.concat(area));
			System.out.println("*******************************");


			System.out.println(" QUESTION 17 ");
			System.out.println("output \t: " + fruit.indexOf("p", 2));
			System.out.println("*******************************");


			System.out.println(" QUESTION 18 ");
			System.out.println("output \t: " + fruit.indexOf(color));
			System.out.println("*******************************");


			System.out.println(" QUESTION 19 ");
			System.out.println("output \t: " + campus.indexOf(campus, 3));
			System.out.println("*******************************");


			System.out.println(" QUESTION 21 ");
			System.out.println("output \t: " + fruit.lastIndexOf("p"));
			System.out.println("*******************************");


			System.out.println(" QUESTION 22 ");
			System.out.println("output \t: " + fruit.lastIndexOf("e", 4));
			System.out.println("*******************************");


			System.out.println(" QUESTION 23 ");
			System.out.println("output \t: " + area.lastIndexOf(area));
			System.out.println("*******************************");


			System.out.println(" QUESTION 24 ");
			System.out.println("output \t: " + color.lastIndexOf(area, 2));
			System.out.println("*******************************");


			System.out.println(" QUESTION 25");
			System.out.println("output \t: " + hotel.length());
			System.out.println("*******************************");


			System.out.println(" QUESTION 29 ");
			System.out.println("output \t: " + color.replace("e", "d"));
			System.out.println("*******************************");


			System.out.println(" QUESTION 35 ");
			System.out.println("output \t: " + fruit.startsWith("A"));
			System.out.println("*******************************");


			System.out.println(" QUESTION 37 ");
			System.out.println("output \t: " + color.substring(3));
			System.out.println("*******************************");


			System.out.println(" QUESTION 38 ");
			System.out.println("output \t: " + hotel.substring(2, 3));
			System.out.println("*******************************");


			System.out.println(" QUESTION 40 ");
			System.out.println("output \t: " + school.toLowerCase());
			System.out.println("*******************************");


			System.out.println(" QUESTION 43 ");
			System.out.println("output \t: " + area.toUpperCase());
			System.out.println();


			System.out.println(" QUESTION 45 ");
			System.out.println("output \t: " + hotel.trim());
			System.out.println();
			
		}
	}

/*
  QUESTION 1 
output 	: p
*******************************
 QUESTION 5 
output 	: BCAS Campus Jaffna
*******************************
 QUESTION 17 
output 	: 2
*******************************
 QUESTION 18 
output 	: -1
*******************************
 QUESTION 19 
output 	: -1
*******************************
 QUESTION 21 
output 	: 2
*******************************
 QUESTION 22 
output 	: 4
*******************************
 QUESTION 23 
output 	: 0
*******************************
 QUESTION 24 
output 	: -1
*******************************
 QUESTION 25
output 	: 9
*******************************
 QUESTION 29 
output 	: Rdd
*******************************
 QUESTION 35 
output 	: true
*******************************
 QUESTION 37 
output 	: 
*******************************
 QUESTION 38 
output 	: l
*******************************
 QUESTION 40 
output 	:    royal institute 
*******************************
 QUESTION 43 
output 	: JAFFNA

 QUESTION 45 
output 	: Valampuri
 
 */


