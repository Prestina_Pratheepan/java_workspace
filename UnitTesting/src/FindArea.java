
public class FindArea {
	
	public double triangle(double base,double height) {
		double area = 0.5 * base * height;
		return area;
	}
	
	public double rectangle(double width, double height) {
		
		return width*height;
	}
	
	public double square(double length) {
		
		return Math.pow(length, 2);
	}
		
	public double circle(double radius) {
		double area = Math.PI*Math.pow(radius, 2);
		return area;
		
	
	}

}
