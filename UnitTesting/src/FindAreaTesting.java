import static org.junit.Assert.assertEquals;

import org.junit.Test;

import junit.framework.Assert;

public class FindAreaTesting {
	
	FindArea findArea= new FindArea();
	
	@Test
	public void circle_Test() {
		double radius = 7;
		double expected = 153.93804002589985;
		double actual = findArea.circle(radius);
		
		
		Assert.assertEquals(153.93804002589985,findArea.circle(radius));
		
	}
	

	@Test
	
	public void circle_Test_withSero() {
		
		Assert.assertEquals(0.0,findArea.circle(0));
	}
	

	@Test
	
	public void rectangle_Test() {
		Assert.assertEquals(50.0, findArea.rectangle(10,5));
	}
	}
	