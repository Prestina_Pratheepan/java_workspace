package bcas.pro.string;

/**
 * To perform basic string methods
 * @author Prestina
 * @since 20.05.2020
 * @version 2.0.0v
 *
 */
public class LettersOfaStringVariable {
	
	/**
	 * 
	 * Main method of this program 
	 * @param args for input arguments
	 */
	
	public static void main(String[] args) {
		
		/**
		 * variable length, length of the word
		 */
				String name = "prestina";
				int length = name.length();
				
		/**
		 * 
		 * print the letters of a string variable
		 * statements given below
		 * 
		 */
				
				for(int i = 0; i < length; i++) {
					System.out.println(name.charAt(i));
				}
	}

}
